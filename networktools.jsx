import { pm } from '../turtleos-pm/pm';

function resolveUrl(url) {

    let vfs = pm.getState().vfs;
    let d = '';
    try {
        let file = vfs.readFileSync('/etc/hosts');
        d=file.data.content;
        d=JSON.parse(d);
    } catch(e) {
        return url;
    }
    if(url.indexOf('://')===-1) {
        return url;
    }
    //url contains params but no end / replace ? with /
    if (url.indexOf('/?') === -1) url.replace('?', '/?');

    let resolved = url;

    for ( let route of d.routes ) {
        console.log(route)
        console.log(resolved)
        resolved = resolved.replace(new RegExp(route.origin, 'g'), route.to);
        console.log(resolved)
    }

    return resolved;

}

function isNetworkEnabled() {
    return pm.getState().io.network;
}

export {
    resolveUrl,
    isNetworkEnabled
}
