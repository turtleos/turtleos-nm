import React from 'react';

import { resolveUrl, isNetworkEnabled } from './networktools';
import iconturtlenoconnection from '../../assets/turtle-assets/turtle-noconnection.png';
import { pm } from '../turtleos-pm/pm';

function WebView({config, url, onChange=()=>{}}) {
    const wv = React.useRef(null);

    if (!isNetworkEnabled()) {
        return (
            <div style={{
                height:'100%',
                width:'100%',
                display:'flex',
                background:'#fff',
                alignItems:'center',
                justifyContent:'center',
                flexDirection:'column'
            }}>
              <img alt="No Connection" src={iconturtlenoconnection} style={{
                  minWidth:'30%',
                  width:'10rem',
                  maxWidth:'50%',
                  maxHeight:'90%'
              }}/>
              <p>{pm.getState().lang.translate('dialog.noconnection')}</p>
            </div>
        );
    }

    if(wv.current) {
      wv.current.onsrcchange=onChange;
    }
    return (
      <iframe
              ref={wv}
              {...config}
        onLoad={()=>{
            if(wv.current) {
              console.log(wv.current.load)
              wv.current.onsrcchange=onChange;
            }
        }}
              frameBorder="0"
              title={resolveUrl(url)}
              src={resolveUrl(url)}
              onsrcchange={(e)=>console.log(e)}
              is={"x-frame-bypass"}
      ></iframe>
    );
}

export {
    WebView
}
